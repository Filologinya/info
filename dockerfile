FROM ubuntu:22.04

# Install latex creation related packages
RUN apt-get update && apt-get install --yes --no-install-recommends \
    texlive-fonts-recommended \
    texlive-latex-extra \
    texlive-full \
    dvipng \
    texlive-base \
    texlive-pictures \
    texlive-lang-cyrillic \
    texlive-science \
    latex2html \
    pandoc \
    cm-super 

# Addons and fonts
RUN apt-get install --yes --no-install-recommends \
    latex-cjk-all \
    lmodern

COPY src /code

WORKDIR /code
ENTRYPOINT /bin/bash ./scripts/build-info -v 

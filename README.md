# Info
[Wegpage](https://filologinya.gitlab.io/info/site-home.html)

## Usage

Use `./local-build.sh` for local usage and generating files to `/public` dir. 

`build-info` script manage whole repository. 

1. Substitute `--address` for you repository or hostname.
2. Store latex and markdown sources in folders you pick in ./src/scripts/build-info.conf
3. gif and png are supported as markdown sources

## Plans for repository

- [x] Build cv from latex
- [x] Build web-pages from markdown
- [x] Make unified script for repo
- [ ] Add available markdown sources extentions to config


#!/bin/sed

# from:
#   \href{https://linkedin.com/in/bunatin}{linkedIn/bunatin}
# to:
#   [{linkedIn/bunatin}]: <{https://linkedin.com/in/bunatin}>
s/\(\\href\)\({.\+}\)\(.\+\)/[\3]: <\2>/g;

# remove {} brackets
s/\({\|}\)//g;

# remove leading whitespaces and 'link: ' garbage
s/^[ \t]*//;
s/link: //g;

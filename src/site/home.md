[**Home**][site/home.md]
[*Source*][cv page]

### Filologinya

> **Contents**:
> [*Keystones*](#keystones)
> [*Articles*](#articles)
> [*Projects*](#projects)
> **other**:
> [*css template*](https://github.com/bndp/beautiful-markdown)
>

> ##### Summary
> Software engineer student at [HSE University](https://www.hse.ru/en/)
> 
> ##### CV
> 
> [Russian][cv/ru.tex]
>

> ##### Contact me
>
> I'm pretty scrupulous about distractions, so I prefer mail 
>
> [amigunova52@gmail.com][amigunova52@gmail.com]
>
> ![](site-busy-stressed.gif)

## Keystones

> *Some quote*
> 

and thougts

## Articles

Reading about technology

1. [Example][articles/Example.md]

## Projects

- [Link compressor][Link compressor] created with FastAPI and Redis

#### *links*

- [Tom Randall Blog](https://randy.gg/)

--------------------------

> © Filologinya 2023


